import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { WakeUpPage } from '../wake-up';
import { CodePage } from '../code';
import { FuturePage } from '../future';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = CodePage;
  tab3Root = WakeUpPage;
  tab4Root = FuturePage;

  constructor() {

  }
}
