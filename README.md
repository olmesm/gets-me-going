# Gets me going

An app created as a quick demonstration of what gets me going in the day.

<https://olmesm.gitlab.io/gets-me-going/>

## To Start

```sh
# Get correct node
nvm use

# Install deps
yarn

# To dev
yarn ionic:serve

# To deploy PWA
git push

# To package
## N/A
```

## Features

- CI/CD
- Ionic 2 Framework
- iOS and Android

## Resources

- [Gitlab](https://gitlab.com)
- [Ionic 2](https://ionic.io)
